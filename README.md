## GEESE - An efficient parallel implementation for GEnomE SEgmentation

This project provides a C++ implementation of the IMP algorithm for sequence segmentation by M. Višnovská, T. Vinar and B. Brejová ([DNA sequence segmentation based on local similarity, ITAT 2013 Proceedings, 36-43](http://ceur-ws.org/Vol-1003/36.pdf "here")). It supports parallel computation using OpenMP libraries and therefore is suitable for processing very large genomic datasets. 

#### License

Copyright (C) 2019 Michel T. Henrichs, Diego P. Rubert, and Daniel Doerr
    
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
   
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
   
You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


#### The genome segmentation problem

Informally, the objective of genome segmentation is the decomposition of a DNA sequence into classes of non-overlapping fragments, called *atoms*. To this end, genome segmentation takes as input pairwise alignments of the DNA sequence onto itself and requires that no alignment boundary lies within any of the created atoms. Note that the genome segmentation problem for multiple DNA sequences is simply defined as the segmentation problem of the concatenated DNA sequences.

A trivial segmentation would establish every single character of the input sequence into an atom of its own, thus satisfying the stated criteria. To avoid such meaningless segmentation, a minimal length requirement is imposed on the constructed atoms. Any nucleotide that is not covered by an atom resides in a *waste region*. The figure below shows an example of a segmentation of two DNA sequences. The objective of the *genome segmentation problem* (GSP) is the construction of a segmentation that minimizes the total number of nucleotides located in waste regions.  In 2013, Višnovská and colleagues have proven its intractability and devised a heuristic called IMP for its solution.

<div align="center">
![Example of a segmentation of two DNA sequences.](img/example_seg.png "Example of a segmentation of two DNA sequences")<br>  
<i>Example of a segmentation of two DNA sequences</i>
</div>


#### Compilation

GEESE requires OpenMP libraries, an API specification for parallel programming.  The libraries for compiling with OpenMP are usually distributed in a package named `libomp-dev` or similar. The ones for running code with OpenMP are usually in a package named `libomp5` or similar. These libraries come installed by default in many systems, therefore the code can be compiled without any additional step by:

```
cd src
make
```

However, sometimes they are not in the system by default. For instance, in Ubuntu systems they can be installed with:

```
sudo apt-get install libomp-dev libomp5
```

Or in Debian systems:

```
apt-get install libomp-dev libomp5
```

As for macOS systems, compiling should work if you have a Xcode installed in your system, try to compile normally as above (by <tt>make</tt>).


#### How to use

The program binary is named `geese` and its usage is as follows:

```
Usage: geese <psl file(s) | list files(s)> [options]

Multiple input psl files (or list files, see --inputNotPsl) may be specified, but always as first arguments.

Optional arguments are specified after their descriptor. The descriptor is NOT case-sensitive. If an optional argument is not invoked, the default value will be used.
--minLength <minLength>: The minimum length an atom must have (default: 250).
--minIdent <minIdent>: Minimum identity an alignment must have to be considered,
  alignments with lower identity will be skipped (default: 80).
--maxGap <maxGap>: The maximum gap length inside of an alignment. If this length is
  exceeded, the alignment will be split in two (default: 13).
--minAlnLength <minAlnLength>: The minimal length an alignment must have to be considered.
  Shorter alignments are ignored (default: 13).
--bucketSize <size>: Size of buckets used to find covering alignments,
  increase if you run out of memory (default: 1000).
--numThreads <num>: Number of threads to run IMP algorithm (default: 1).
--printZeroLines: Print line numbers with blocks of size 0 (default: no).
--inputNotPsl: Each input file is not a psl file. Instead of data, the given files contain
  the path of one psl file per line, which actually contain the data to be read (default: no).
```

The input files must come first (see [PSL Input format](#psl-input-format) section), followed by the other program arguments. The input files are PSL files or one file with the actual list of PSL files, one per line (if the option `--inputNotPsl` is used). This is useful in cases where the number of input files is too large and cannot be passed as command line arguments.


#### PSL Input format

The [PSL File Format](https://www.ensembl.org/info/website/upload/psl.html) is defined as follows. The lines represent alignments, and each one consists of 21 space-separated fields. Positions in PSL files are 0-based `[start,end)`. A line can also be a comment, in this case it starts with `#`. List of psl fields:

* `matches`: number of bases that match and which are not part of repeats
* `misMatches`: number of bases that don't match
* `repMatches`: number of bases that match but are part of repeats
* `nCount`: number of "N" bases
* `qNumInsert`: number of inserts in query
* `qBaseInsert`: number of bases inserted in query
* `tNumInsert`: number of inserts in target
* `tBaseInsert`: number of bases inserted in target
* `strand`: "+" or "-" for query strand. For translated alignments, second "+" or "-" is for target genomic strand.
* `qName`: query sequence name
* `qSize`: query sequence size.
* `qStart`: alignment start position in query
* `qEnd`: alignment end position in query
* `tName`: target sequence name
* `tSize`: target sequence size
* `tStart`: alignment start position in target
* `tEnd`: alignment end position in target
* `blockCount`: number of blocks in the alignment (a block contains no gaps)
* `blockSizes`: comma-separated list of sizes of each block. If the query is a protein and the target the genome, blockSizes are in amino acids.
* `qStarts`: comma-separated list of starting positions of each block in query
* `tStarts`: comma-separated list of starting positions of each block in target

Example of how coordinates work in psl files:

```
Here is a 61-mer containing 2 blocks that align on the minus strand and 2 blocks
that align on the plus strand (this sometimes happens due to assembly errors):
"""
EXAMPLE = """\
0         1         2         3         4         5         6 tens position in query
0123456789012345678901234567890123456789012345678901234567890 ones position in query
                      ++++++++++++++                    +++++ plus strand alignment on query
    ------------------              --------------------      minus strand alignment on query
0987654321098765432109876543210987654321098765432109876543210 ones position in query negative strand coordinates
6         5         4         3         2         1         0 tens position in query negative strand coordinates

Plus strand:
     qStart=22
     qEnd=61
     blockSizes=14,5
     qStarts=22,56

Minus strand:
     qStart=4
     qEnd=56
     blockSizes=20,18
     qStarts=5,39
```


#### Output format

In the output format, each line can be a comment starting with `#` or contain information about one atom. In this case, the line has 6 tab-separated fields:

* `name`: chromosome name
* `atom_nr`: number of the atom
* `class`: class to which the atom belongs to 
* `strand`: strandness of the atom
* `start`: start position in its chromosome
* `end`: end position in its chromosome

Positions are 0-based `[start,end)`.


#### Memory consumption

This program is capable of processing millions of alignments very quickly. However, fitting all the data in memory may not be always possible. As described in Section [PSL Input format](#psl-input-format), each alignment is composed of 21 fields. The last three fields (<tt>blockSizes</tt>, <tt>qStarts</tt> and <tt>tStarts</tt>) are actually lists of values. Such values are usually small and fit in unsigned int arrays. Such arrays, however, can be very large and are responsible for most of the memory usage in the program. Therefore, this implementation uses by default unsigned int arrays for storing those lists. Nevertheless, in some cases where the input genomes have excessively large alignments, unsigned ints cannot fit those values, resulting in "*Cannot fit this number...*" exceptions or *segmentation faults*. In this case, there are two alternatives:

* Increase the variable size of elements in the lists <tt>blockSizes</tt>, <tt>qStarts</tt> and <tt>tStarts</tt> of alignments
* Remove or split manually long alignments

To increase the size of elements in lists, open the file `AlignmentRecord.h` and change the line `#define BLOCKS_SIZE BLOCKS_UINT` to `#define BLOCKS_SIZE BLOCKS_ULONG`, then compile everything again. In addition, the program `GetMaxBlockSizeAndLocalStart` was made to help checking which alignments have values too large to fit in those lists. It takes into account the current value in `#define BLOCKS_SIZE` and can be built by `make GetMaxBlockSizeAndLocalStart`. After that, run it with the **exact same parameters** that would be used with the <tt>geese</tt> binary, it will scan the PSL files and check which lines/alignments contain values that after processed (because some alignments can be split by <tt>geese</tt> depending on the parameters) will not fit in the current `#define BLOCKS_SIZE` variable type. This program can also help finding whether there are just few problematic lines (and that can be manually dealt with) or not. 

On the other hand, sometimes all elements in <tt>blockSizes</tt>, <tt>qStarts</tt> and <tt>tStarts</tt> lists fit in unsigned ints but there are too many alignments and not enough RAM to keep all of them in memory. The program `GetMaxBlockSizeAndLocalStart` can be used to check the largest value in those lists. By the time this text was written, the largest value that could be stored in an unsigned short variable was 65535 for most of 64bit machines. Thus, if the largest value in such lists reported by <tt>GetMaxBlockSizeAndLocalStart</tt> is less than or equal to 65535, opening the file `AlignmentRecord.h`, changing the line `#define BLOCKS_SIZE BLOCKS_UINT` to `#define BLOCKS_SIZE BLOCKS_USHORT` and then compiling everything again can save a substantial amount of memory. The same change can be made if it is known that those values fit in unsigned shorts and we want to speedup the <tt>geese</tt> program.

Finally, we assume lines are at most 1MB long, that is, 1024 * 1024 characters. A segmentation fault in the <tt>geese</tt> or <tt>GetMaxBlockSizeAndLocalStart</tt> binaries probably will occur if lines in PSL input files are larger than that. In such case, the constant value `static const unsigned int MAX_LINE` in `InputParser.h` must be increased to a suitable value. The longest line can be found by the `wc -L psl_file` command. 

Please note that the contents of this section can be not entirely true depending on the architecture of the system.


#### Other credits

Part of the avatar logo for this git project is "geese" by Sarah JOY from the Noun Project.
